const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "development",
    devServer: {
        static: path.resolve("./dist"),
    },

    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        }]
    },

    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [new HtmlWebpackPlugin()]
};